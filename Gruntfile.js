module.exports = function ( grunt ) {

	grunt.initConfig( {
		pkg: grunt.file.readJSON( 'package.json' ),
		htmlmin: {
			dist: {
				options: {
					removeComments: true,
					collapseWhitespace: true
				},
				files: [ {
						expand: true,
						cwd: 'src/html',
						src: [ '**/*.html', '*.html' ],
						dest: 'build'
					} ]
			}
		},
		sass: {
			dist: {
				options: {
					style: 'compressed'
				},
				files: [ {
						expand: true,
						cwd: 'src/scss',
						src: [
							'*.scss'
						],
						dest: 'build/css',
						ext: '.min.css'
					} ]
			}
		},
		imagemin: {
			dist: {
				files: [ {
						expand: true,
						cwd: 'src/images/',
						src: [ '*.{png,jpg,gif}' ],
						dest: 'build/images/'
					} ]
			}
		},
		googlefonts: {
			dev: {
				options: {
					fontPath: 'build/fonts/',
					cssFile: 'src/scss/fonts/_fonts.scss',
					httpPath: '../fonts/',
					formats: {
						eot: true,
						woff: true,
						svg: true
					},
					fonts: [
						{
							family: 'Rokkitt',
							styles: [
								400, 700
							]
						},
						{
							family: 'Lato',
							styles: [
								300, 400
							]
						}
					]
				}
			}
		},
		watch: {
			grunt: {
				files: [ 'Gruntfile.js' ]
			},
			sass: {
				files: [ 'src/scss/*.scss' ],
				tasks: [ 'sass' ]
			},
			htmlmin: {
				files: [ 'src/html/*.html' ],
				tasks: [ 'htmlmin' ]
			},
			imagemin: {
				files: [ 'src/images/*' ]
			}

		}
	} );

	grunt.loadNpmTasks( 'grunt-google-fonts' );
	grunt.loadNpmTasks( 'grunt-contrib-sass' );
	grunt.loadNpmTasks( 'grunt-contrib-watch' );
	grunt.loadNpmTasks( 'grunt-contrib-htmlmin' );
	grunt.loadNpmTasks( 'grunt-contrib-imagemin' );

	grunt.registerTask( 'default', [
		'watch'
	] );

	grunt.registerTask( 'build', [
		'htmlmin', 'googlefonts', 'sass', 'imagemin'
	] );

};