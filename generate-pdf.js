const convertHTMLToPDF = require("pdf-puppeteer");
const fs = require('fs');
var callback = function (pdf) {
    fs.writeFileSync('build/Gagan-Deep-Singh-Resume-WordPress-Developer.pdf', pdf);
};
var options = {
	format: 'A4',
	margins: {
		top: "0.4in",
		right: "0.4in",
		bottom: "0.4in",
		left: "0.4in"
	}
};
content = String(fs.readFileSync('./build/index.html'));
convertHTMLToPDF(content, callback, options);

    
  